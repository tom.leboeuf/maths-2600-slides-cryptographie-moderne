import hashlib
import os
from pathlib import Path
from pprint import pprint

passwords = (Path(__file__).parent / "2151220-passwords.txt").read_text().splitlines()

users_and_password_hashes = {
    'angry_stonebraker': '0b7df2d1f6cec1884e8f87c7cf83cdafd2b30c61b1cfba3f553833a6cfc1b1b6',
    'blissful_cartwright': '9c4a0cfab35f1a05635d51709746363a7f3fbc8d4eab34c7c86b73370eae94ed',
    'brave_rubin': '7ff92d2bd88e24d8493ae350304dc2b0c1dbc9fd46689605cdfa6398fc5cee9c',
    'clever_ramanujan': '1c211869bbedafe524f6a7e8a13d4cb7f5a3f0cf58eebbd32c048bb99eccee76',
    'cool_solomon': 'cfb26269ea232f2af0c5d0f320593cff324396e215dccf0139a42ac2d275f817',
    'determined_jemison': 'ec65580434eb281890040efe6a3cbfb749cdb85dbd7e9de12b8441aaa46b5ef4',
    'dreamy_saha': '7e00bca0d86b01aad63ed57cf457a872ce9e0c23479f28df63b315163d85e4b0',
    'eager_goldstine': '06f52b4a679702ad8613fa8a7a0986e49544739c047fc8ce1f60d6637dbe9d5a',
    'elastic_chebyshev': '43c0bedea1602f7426c923aab707e796be9c7297713621803deba0163234cc71',
    'exciting_goodall': '76c3a331963ccc93ba70005a1037e5d88672c8b30f72ece368ed2471047d6bb5',
    'festive_brown': 'af2c49b75ec9548e92dd3741019c593f3f7c7fe9fcc70cb6706749809b3a4bb3',
    'flamboyant_gould': '52df6f9fa74583ed8e80308e67e960c36d05f911f6f71575179cb452a79fa195',
    'flamboyant_haslett': '7e3e22dd11c1650544769e3c754dc5551a43f26da486992a787f9279fcb6061b',
    'infallible_torvalds': '32d96bb640031af21e666ad8ca43ba3d18bb9bd4b1367835f021eb916161569a',
    'jolly_panini': '8c0fb38186e5caef5f3f3d7be26a1aca061c56e06b04e5adaedd95beba719829',
    'keen_goldstine': 'c97477abf7ff2125397203a8bb079d61f803f94064cb0ad20fbdb2b0ad131aae',
    'keen_mclean': '886167745219b5d809c0394d97b8e4a0f8f8292a1b1449dbe4c0bee559349438',
    'objective_bhaskara': '4a83004f5c7c3fc6e44ee5b3f97854075910273aaa2062ed0793bb075b9aaf2b',
    'objective_elbakyan': '0d8f7558ffcbb3599a34c21f6b507d0c0d15a49310c37273f55fd509539a0da9',
    'optimistic_lehmann': 'afcf07ad205cefe36aa1463b421e8e07c5c2ab904796f7ae8bb8e1e0464f222e',
    'relaxed_moser': '030abcbda015a6be0dba9abdd8b7764a2a4ed6977acdf3c413f217028b528d49',
    'reverent_easley': '3d3e4e2431f4ba144f8eed93d0467568d1163e2712da5b18a2745d6dfb62727a',
    'sad_pare': '4e9a2585dcc974cb6284df24baacbbc8889302909a606d7413b8e95046730918',
    'serene_pare': '7eb0720ae399a3f13cedaab3dfe49128c5652fad2ea946b855a143b47aef670e',
    'sharp_merkle': '89a7f6ab2813edbd4027c133bf4f6d50b2d559bc4cdf326820b0947f45e896a5',
    'sleepy_moore': 'f84ea44e903545116a09619b2bf9767572f28a574a07de7b74935192dd9f3f9d',
    'sweet_torvalds': '44ab7d6cc6f365973e93bee275753deeb629236cafef5c668bc29b352c9695bb',
    'tender_hertz': '2a92e17de506562b2860dae8b102b669287f9d53cdefe98fc68c17ac8fc89c20',
    'wizardly_brahmagupta': 'f09259c4caadd575e1c6dbda3df0e21a9f1472c9ab3b75373391edd3b074dd53',
    'wizardly_robinson': '7f6fc3cc014d30b4923304d797728ee7aa233a655e3fa41d8ecb26ff58006e1f',
    'youthful_lichterman': '99c7f2fa7031955255c8d2a6a681cc3e96df7dbe29935a55be3e05064caff895',
    'youthful_williamson': '5850d36efa1a717162e43c7fd0cbe0caa77ba2b5fb89bbfd272363559991c44a'
}

users_and_salt = {}

def generate_users_and_password_hashes(count = 32):
    from random import Random

    from names_generator import generate_name

    rng = Random()

    users_and_password_hashes = {
        generate_name(): hashlib.sha3_256(rng.choice(passwords).encode()).hexdigest() for _i in range(count)
    }
    pprint(users_and_password_hashes)

def attack_passwords() -> dict:
    users_and_passwords = {}

    # Doit calculer le mots de passe de chaque utilisateur grace à une attaque par dictionnaire

    pprint(users_and_passwords)

    return users_and_passwords


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def fix_password_hashes_with_salt() -> None:
    users_and_passwords = attack_passwords()

    users_and_salt = {}
    users_and_new_password_hash = {}

    # A partir de users_and_passwords, doit calculer pour chaque utilisateur un salt (utilisez la fonction random_salt)
    # et doit calculer de nouveau hash combinant le salt et le password en clair (hashlib.sha3_256(salt + password))

    # Affiche les nouveaux dict, a copier coller dans les dict en haut du fichier
    pprint(users_and_new_password_hash)
    pprint(users_and_salt)


def authenticate(user: str, password: str) -> bool:
    # Doit renvoyer True si l'utilisateur a envoyé le bon password, False sinon
    # en utilisant les salt et nouveau hash calculés par fix_password_hashes_with_salt()
    pass

# Ce test doit passer après implémentation complète
assert authenticate('youthful_lichterman', 'frenchrevolution')
